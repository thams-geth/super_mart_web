import 'package:flutter/material.dart';
import 'package:super_mart_admin/screens/category/category_screen.dart';
import 'package:super_mart_admin/screens/dashboard/dashboard.dart';
import 'package:super_mart_admin/screens/login/login_screen.dart';
import 'package:super_mart_admin/screens/products/products_screen.dart';

final Map<String, WidgetBuilder> routes = {
  RouteNames.loginScreen: (context) => LoginScreen(),
  RouteNames.dashBoardScreen: (context) => DashBoard(),
  RouteNames.categoryScreen: (context) => CategoryScreen(),
  RouteNames.productsScreen: (context) => ProductsScreen(),
};

class RouteNames {
  static final String loginScreen = "/login";
  static final String dashBoardScreen = "/dashboard";
  static final String categoryScreen = "/category";
  static final String productsScreen = "/products";
  static final String historyScreen = "/history";
}
