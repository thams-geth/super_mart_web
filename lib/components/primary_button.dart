import 'package:flutter/material.dart';
import 'package:super_mart_admin/constants.dart';

class PrimaryButton extends StatelessWidget {
  final String name;
  final VoidCallback press;
  final double width;

  const PrimaryButton({
    Key? key,
    required this.name,
    required this.press,
    this.width = double.infinity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: ElevatedButton(
          onPressed: press,
          style: ElevatedButton.styleFrom(
            padding: const EdgeInsets.symmetric(vertical: 20),
            primary: primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          child: Text(
            name,
            style: TextStyle(
              color: Colors.white,
            ),
          )),
    );
  }
}
