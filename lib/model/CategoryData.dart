class CategoryData {
  final String categoryName;
  final List<String> productList;
  CategoryData({required this.categoryName, required this.productList});
}

List<String> categoryOneproductsList = [
  "Onion",
  "Potato",
  "Tomoto",
  "Lemon",
  "Cucumber",
  "Cabbage",
  "Ginger",
  "Curry Leaves"
];
List<String> categoryTwoproductsList = [
  "Banana",
  "Apple",
  "Orange",
  "Guava",
  "Papaya",
  "Grapes",
  "WaterMelon",
];

List<String> categoryThreeproductsList = [
  "Lays",
  "Milkey Busiket",
  "Coke",
  "Bovanto",
  "Bingo",
  "Treat",
];

List<String> categoryFourproductsList = [
  "Shampoo",
  "Hair dryer",
  "Soap",
  "Shaving set",
  "Domix",
  "Harpic",
];

List categoryList = [
  CategoryData(
      categoryName: "Vegitables", productList: categoryOneproductsList),
  CategoryData(categoryName: "Fruits", productList: categoryTwoproductsList),
  CategoryData(
      categoryName: "Beverages", productList: categoryThreeproductsList),
  CategoryData(
      categoryName: "Personal Care", productList: categoryFourproductsList),
];
