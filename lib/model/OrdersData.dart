class NewOrderData {
  final String orderName, orderType, orderWeight;
  final double orderPrice;
  final List orderitem;

  NewOrderData(
      {required this.orderName,
      required this.orderType,
      required this.orderWeight,
      required this.orderPrice,
      required this.orderitem});
}

class OrderItemData {
  final String itemName, itemWeight;
  final double itemQuantity, itemPrice;
  OrderItemData(
      {required this.itemName,
      required this.itemWeight,
      required this.itemQuantity,
      required this.itemPrice});
}

List orderItemsList = [
  OrderItemData(
      itemName: "Tomoto", itemWeight: "1 kg", itemQuantity: 3, itemPrice: 60),
  OrderItemData(
      itemName: "Lays", itemWeight: "50 gram", itemQuantity: 3, itemPrice: 15),
  OrderItemData(
      itemName: "Rava", itemWeight: "200 gram", itemQuantity: 1, itemPrice: 30),
];

List newOrdersList = [
  NewOrderData(
      orderName: "Thams's Order",
      orderType: "Store pickup",
      orderWeight: "4 kg",
      orderPrice: 115,
      orderitem: orderItemsList),
  NewOrderData(
      orderName: "Vignesh's Order",
      orderType: "Store pickup",
      orderWeight: "10 kg",
      orderPrice: 15,
      orderitem: orderItemsList),
  NewOrderData(
      orderName: "Venkat's Order",
      orderType: "Store pickup",
      orderWeight: "6kg",
      orderPrice: 235,
      orderitem: orderItemsList),
  NewOrderData(
      orderName: "Rajini's Order",
      orderType: "Store pickup",
      orderWeight: "6 kg",
      orderPrice: 2443,
      orderitem: orderItemsList),
  NewOrderData(
      orderName: "Surya's Order",
      orderType: "Store pickup",
      orderWeight: "5 kg",
      orderPrice: 924,
      orderitem: orderItemsList),
  NewOrderData(
      orderName: "Another one Order",
      orderType: "Store pickup",
      orderWeight: "7 kg",
      orderPrice: 2349,
      orderitem: orderItemsList),
  NewOrderData(
      orderName: "The last one  Order",
      orderType: "Store pickup",
      orderWeight: "8 kg",
      orderPrice: 3984,
      orderitem: orderItemsList),
];
