import 'package:flutter/material.dart';
import 'package:super_mart_admin/routes.dart';

import 'drawer_list_tile.dart';

class DrawerMenu extends StatelessWidget {
  const DrawerMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Thamaraiselvan",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text("Super Mart ID :12345")
                ],
              ),
            ),
          ),
          DrawerListTile(
            icon: Icons.ac_unit,
            title: "Dashboard",
            press: () {
              Navigator.pushNamed(context, RouteNames.dashBoardScreen);
            },
          ),
          DrawerListTile(
            icon: Icons.ac_unit,
            title: "Categories",
            press: () {
              Navigator.pushNamed(context, RouteNames.categoryScreen);
            },
          ),
          DrawerListTile(
            icon: Icons.ac_unit,
            title: "Products",
            press: () {
              Navigator.pushNamed(context, RouteNames.productsScreen);
            },
          ),
          DrawerListTile(
            icon: Icons.ac_unit,
            title: "History",
            press: () {},
          ),
          DrawerListTile(
            icon: Icons.ac_unit,
            title: "Logout",
            press: () {},
          ),
        ],
      ),
    );
  }
}
