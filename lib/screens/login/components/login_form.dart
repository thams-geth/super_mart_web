import 'package:flutter/material.dart';
import 'package:super_mart_admin/components/primary_button.dart';

import '../../../routes.dart';

class LoginForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _key = GlobalKey<FormState>();
    return Form(
      key: _key,
      child: Column(
        children: [
          TextFormField(
            decoration: InputDecoration(
                hintText: "Enter your SuperMart id",
                labelText: "SuperMart Id",
                prefixIcon: Icon(Icons.account_circle)),
            validator: (val) {
              if (val!.isEmpty) {
                return "Please enter super mart id";
              }
              if (val.length < 3) {
                return "Please enter valid super mart id";
              }
            },
          ),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(
                hintText: "Enter your password",
                labelText: "Password",
                prefixIcon: Icon(Icons.lock)),
            validator: (val) {
              if (val!.isEmpty) {
                return "Please enter password";
              } else if (val.length < 6) {
                return "Please enter valid password";
              }
            },
          ),
          PrimaryButton(
            name: "Login",
            press: () {
              if (_key.currentState!.validate()) {
                Navigator.popAndPushNamed(context, RouteNames.dashBoardScreen);
              }
            },
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            "Forgot password",
            style: TextStyle(
              decoration: TextDecoration.underline,
            ),
          ),
        ],
      ),
    );
  }
}
