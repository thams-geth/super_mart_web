import 'package:flutter/material.dart';
import 'package:super_mart_admin/screens/dashboard/components/dashboard_body.dart';
import 'package:super_mart_admin/screens/drawer/drawer_menu.dart';

class DashBoard extends StatelessWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerMenu(),
      appBar: AppBar(title: Text("DashBoard")),
      body: DashBoardBody(),
    );
  }
}
