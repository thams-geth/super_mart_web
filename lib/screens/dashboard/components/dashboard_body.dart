import 'package:flutter/material.dart';

import 'in_progress/in_progress.dart';
import 'new_order/new_orders.dart';
import 'out_for_delivery/out_for_delivery.dart';

class DashBoardBody extends StatelessWidget {
  const DashBoardBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(child: NewOrders()),
        SizedBox(width: 15),
        Expanded(child: InProgress()),
        SizedBox(width: 15),
        Expanded(child: OutForDelivery()),
      ],
    );
  }
}
