import 'package:flutter/material.dart';
import 'package:super_mart_admin/components/primary_button.dart';
import 'package:super_mart_admin/components/secondary_button.dart';
import 'package:super_mart_admin/model/OrdersData.dart';

import '../../../../constants.dart';
import 'order_items.dart';

class NewOrderItems extends StatelessWidget {
  final NewOrderData newOrdersData;
  final bool isNewOrder, isInProgress, isOutForDelivery;
  const NewOrderItems({
    Key? key,
    required this.newOrdersData,
    this.isNewOrder = false,
    this.isInProgress = false,
    this.isOutForDelivery = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: primaryColor),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              newOrdersData.orderName,
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            Text("Order Items"),
            OrderItems(),
            Text("Order Type : ${newOrdersData.orderType} "),
            Text("Order Weight : ${newOrdersData.orderWeight} "),
            Text("Order price : ${newOrdersData.orderPrice} "),
            Visibility(
              visible: isNewOrder,
              child: ButtonBar(
                children: [
                  SecondaryButton(name: "Decline", press: () {}, width: 100),
                  PrimaryButton(name: "Accept", press: () {}, width: 100),
                ],
              ),
            ),
            Visibility(
              visible: isInProgress,
              child: PrimaryButton(
                name: "In Progress",
                press: () {},
              ),
            ),
            Visibility(
              visible: isOutForDelivery,
              child: PrimaryButton(
                name: "Out For Delivery",
                press: () {},
              ),
            )
          ],
        ),
      ),
    );
  }
}
