import 'package:flutter/material.dart';

class OrderItems extends StatelessWidget {
  const OrderItems({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(flex: 2, child: Text("Tomoto")),
                    Expanded(child: Text("* 3")),
                    Expanded(child: Text(" = 100"))
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
