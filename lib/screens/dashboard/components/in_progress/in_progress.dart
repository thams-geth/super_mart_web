import 'package:flutter/material.dart';
import 'package:super_mart_admin/model/OrdersData.dart';
import 'package:super_mart_admin/screens/dashboard/components/new_order/new_order_items.dart';

import '../../../../constants.dart';

class InProgress extends StatelessWidget {
  const InProgress({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return SingleChildScrollView(
       child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.white),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Text(
                "In progress",
                style: TextStyle(fontSize: 26, color: primaryColor),
              ),
              SizedBox(
                height: 10,
              ),
              ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: newOrdersList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: NewOrderItems(
                      newOrdersData: newOrdersList[index],isInProgress: true,
                    ),
                  );
                },
              ),
            ],
          ),
        ),
         ),
     );
  }
}
