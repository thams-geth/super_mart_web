import 'package:flutter/material.dart';
import 'package:super_mart_admin/screens/drawer/drawer_menu.dart';
import 'package:super_mart_admin/screens/products/components/products_body.dart';

class ProductsScreen extends StatelessWidget {
  const ProductsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerMenu(),
      appBar: AppBar(title: Text("DashBoard")),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Productsbody(),
      )
    );
  }
}
