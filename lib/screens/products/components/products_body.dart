import 'package:flutter/material.dart';
import 'package:super_mart_admin/screens/products/components/add_products.dart';
import 'package:super_mart_admin/screens/products/components/products_list.dart';

class Productsbody extends StatelessWidget {
  const Productsbody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(flex: 3, child: ProductsList()),
        SizedBox(width: 20),
        Expanded(flex: 5, child: AddProducts()),
        SizedBox(width: 20),
      ],
    );
  }
}
