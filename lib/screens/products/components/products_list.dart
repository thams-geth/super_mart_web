import 'package:flutter/material.dart';
import 'package:super_mart_admin/model/ProductsData.dart';

class ProductsList extends StatelessWidget {
  const ProductsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: double.infinity,
      child: Column(
        children: [
          Text("All Products"),
          ListView.builder(
            shrinkWrap: true,
            itemCount: productList.length,
            itemBuilder: (context, index) =>
                ProductListTile(productsData: productList[index]),
          )
        ],
      ),
    );
  }
}

class ProductListTile extends StatelessWidget {
  final ProductsData productsData;
  const ProductListTile({Key? key, required this.productsData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.asset(
              productsData.productImage,
              width: 100,
              height: 100,
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(width: 10),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(productsData.productName),
              Text("Rs : ${productsData.price}"),
              Text(productsData.productDescription),
              Text("Weight : ${productsData.productName}"),
            ],
          )
        ],
      ),
    );
  }
}
