import 'package:flutter/material.dart';
import 'package:super_mart_admin/model/CategoryData.dart';

class CategoryDetails extends StatelessWidget {
  const CategoryDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Products in category"),
                OutlinedButton(onPressed: () {}, child: Text("ADD"))
              ],
            ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: categoryOneproductsList.length,
              itemBuilder: (context, index) =>
                  CardProductItem(name: categoryOneproductsList[index]),
            )
          ],
        ),
      ),
    );
  }
}

class CardProductItem extends StatelessWidget {
  final String name;
  const CardProductItem({Key? key, required this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(children: [
        Text(name),
        Spacer(),
        Icon(Icons.more_horiz),
        SizedBox(width: 10),
        Icon(Icons.delete)
      ]),
    );
  }
}
