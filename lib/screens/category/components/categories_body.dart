import 'package:flutter/material.dart';
import 'package:super_mart_admin/screens/category/components/category_details.dart';
import 'package:super_mart_admin/screens/category/components/category_list.dart';

class CategoriesBody extends StatelessWidget {
  const CategoriesBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Row(
        children: [
          SizedBox(width: 20),
          Expanded(flex: 3, child: CategoryList()),
          SizedBox(width: 20),
          Expanded(flex: 3, child: CategoryDetails()),
          SizedBox(width: 20),
          Expanded(flex: 3, child: Container()),
          SizedBox(width: 20),
        ],
      ),
    );
  }
}
