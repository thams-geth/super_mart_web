import 'package:flutter/material.dart';
import 'package:super_mart_admin/model/CategoryData.dart';

class CategoryList extends StatefulWidget {
  const CategoryList({Key? key}) : super(key: key);

  @override
  _CategoryListState createState() => _CategoryListState();
}

class _CategoryListState extends State<CategoryList> {
  TextEditingController _textFieldController = TextEditingController();
  Future<void> _displayAddCategoryDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Add category'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  valueText = value;
                });
              },
              controller: _textFieldController,
              decoration: InputDecoration(hintText: "Text Field in Dialog"),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('CANCEL'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  setState(() {
                    valueText = "";
                    categoryList.add(
                        CategoryData(categoryName: valueText, productList: []));
                    Navigator.pop(context);
                  });
                },
              ),
            ],
          );
        });
  }

  late String valueText;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("All Categories"),
                OutlinedButton(
                    onPressed: () {
                      _displayAddCategoryDialog(context);
                    },
                    child: Text("ADD"))
              ],
            ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: categoryList.length,
              itemBuilder: (context, index) => CategoryItem(
                categoryData: categoryList[index],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CategoryItem extends StatelessWidget {
  final CategoryData categoryData;
  const CategoryItem({
    Key? key,
    required this.categoryData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Card(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Text(categoryData.categoryName),
        ),
      ),
    );
  }
}
