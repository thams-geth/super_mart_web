import 'package:flutter/material.dart';
import 'package:super_mart_admin/screens/category/components/categories_body.dart';
import 'package:super_mart_admin/screens/drawer/drawer_menu.dart';

class CategoryScreen extends StatelessWidget {
  const CategoryScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerMenu(),
      appBar: AppBar(title: Text("DashBoard")),
      body: CategoriesBody(),
    );
  }
}
